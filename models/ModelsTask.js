const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
    Title: {
        type: String,
         required: [true, 'A Task must have a title'],
         unique: true
        
    },
    Description: {
        type: String,
        trim: true
    },
    Date: {
        type: Date,
        default: Date.now(), 
       
    },
    assignesTo: {
        type: String,
        trim: true
    },
    Priority: {
        type: String,
        required: [true, 'A Task must have a Priority'],
        Tnum: {
            values: ['easy', 'medium', 'High'],
            message: 'Priority is either: easy, medium, High'
        }
    },

    Duration:{
    /*     type: Date,
        default:date.now()+ data.last() */
        type: String,
        unique: true
    }
})

const Task = mongoose.model('tasks', TaskSchema);

module.exports = Task;