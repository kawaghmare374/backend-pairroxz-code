const express = require('express');
const Controller = require('../Controller/TaskController');
const router = express.Router();

//create Task
router.route('/createnewTask').post(Controller.createnewTask)
router.route('/getalltask').get(Controller.getAllTask)
router.route('/updateTask/:id').patch(Controller.updateTask) 
router.route('/deleteTask/:id').delete(Controller.deleteTask)


module.exports = router;