const Task = require('../models/ModelsTask');

///////////////////////create Task///////////////////////

exports.createnewTask = async (req, res) => {
      try{
       
   // await Task.deleteMany();
   
     const newTask = await Task.create(req.body);
        console.log(req.body);
       res.status(201).json({
             status: 'success',
             data: {
                Task: newTask
              }
          })
      } catch(err){
          console.log(err);
          res.status(400).json({
              status: 'fail',
              message: err
         }) 
       } 
    }

    exports.getAllTask = async (req, res) => {
        try{
           
            const task = await Task.find();
            console.log(task);
            res.status(200).json({
                status: 'success',
                result: task.length,
                data: {
                    task
                }
            })
        } catch(err){
            console.log(err);
            res.status(400).json({
                status: 'fail',
                message: err
            })
        }
    }
    
    

    exports.updateTask = async (req, res) => {
  try{
       const task = await Task.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true});
       res.status(200).json({
           status: 'success',
           data: {
               task
           }
       })
   } catch (err){
       console.log(err);
       res.status(404).json({
           status: 'fail',
            message: err
        })
    }
 }

 exports.deleteTask = async (req, res) => {
    try{
       const task = await Task.findById(req.params.id);
           if(!task){
              res.status(404).json({
                  status: 'fail',
                  message: 'Task not found'
               })
            }
          await Task.findByIdAndDelete(req.params.id);
          res.status(200).json({
               status: 'success',
               message: 'task Deleted Succefully'
           })
        } catch (err){
            res.status(404).json({
                status: 'fail',
                message: err
            })
        }
 }
    


