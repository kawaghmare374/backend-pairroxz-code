
const express = require('express');
require('dotenv').config();
var morgan = require('morgan')
const Mongoose= require('mongoose');

const TaskRoutes = require('./routes/TaskRoutes');
const app = express();
var cors = require('cors')






Mongoose.connect('mongodb://localhost:27017/Task',{useNewUrlParser:true,useUnifiedTopology:true}).then(con=>{

    console.log("database Sucessfully Connected");
}).catch(err=>{
       console.log(err);
       console.log("not connected",err);
       

})

/////////////////////////////////////
app.use(express.json());
app.use(cors());

app.use(function(req, res, next){
    console.log("hallo from middleware");
    next();
})

if(process.env.NODE_ENV === 'development'){
    app.use(logger('tiny'));
}
app.use('/api/v1/Task', TaskRoutes);

////////////////////////server connection/////////////////////////
const PORT = process.env.PORT || 8001;
app.listen(PORT, function(){
    console.log(`server is up on port ${PORT}`);
})